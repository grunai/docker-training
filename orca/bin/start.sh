#!/usr/bin/env sh
set -e
chmod +x ./orca
set -x
exec ./orca --no-daemon
