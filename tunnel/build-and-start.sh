#!/bin/bash

BOXID=${BOXID:-1}

docker build -t tunnel --build-arg BOXID=$BOXID .
docker run --rm -it -p 8081:8081 tunnel
